package Controller;

import Model.IModel;
import View.IView;

public class Controller {
	private IView view;
	private IModel model;
	
	public Controller(IView view, IModel model){
		this.view = view;
		this.model = model;
	}
	
	public void start() {
		
	}
	
	public IView getView() {
		return this.view;
	}
	
	public IModel getModel() {
		return this.model;
	}
}
