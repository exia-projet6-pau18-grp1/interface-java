package View;

import java.awt.Dimension;
import java.awt.Rectangle;

public interface IView {
	public Dimension getDimension();
	public void setDimension(Dimension dimension);
	public void setDisplayFrame(Rectangle displayFrame);
	public void update();

}
