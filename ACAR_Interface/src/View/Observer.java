package View;

import java.io.BufferedReader;

import gnu.io.SerialPortEvent;

public class Observer {
	private static final String PORT_NAME = "COM5";
	private BufferedReader input;
	private static final int TIME_OUT = 200;
	private static final int DATA_RATE = 9600;
	
	public void initialize() {
		
	}
	
	public synchronized void close() {
		
	}
	
	public synchronized void serialEvent(SerialPortEvent oEvent) {
		
	}
	
	public void run() {
		
	}
}
