package View;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.JPanel;

import Model.MapObject;

public class BoardPanel extends JPanel{
	private Rectangle displayFrame;
	private Dimension dimension;
	private Observer noCar;

	protected BoardPanel() {
		
	}
	
	public void paintComponent(Graphics graphics) {
		
	}
	
	public void update(Observer observable, MapObject mapObject) {
		
	}
	
	public void setDimension(Dimension dimension) {
		
	}
	
	public Dimension getDimension() {
		return dimension;
	}

	public Rectangle getDisplayFrame() {
		return displayFrame;
	}

	public void setDisplayFrame(Rectangle displayFrame) {
		this.displayFrame = displayFrame;
	}
	
	public Observer getNoCar() {
		return noCar;
	}
}
