package View;

import java.awt.Dimension;
import java.awt.Rectangle;

import javax.swing.JFrame;

public class BoardFrame extends JFrame implements IView {
	private int defaultWidthSize;
	private int defaultHeightSize;
	private Dimension dimension;
	private BoardPanel boardPanel;
	
	BoardFrame(){
		
	}

	public Dimension getDimension() {
		return dimension;
	}

	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
		
	}

	public void setDisplayFrame(Rectangle displayFrame) {
		
	}
	
	public Rectangle getDisplayFrame() {
		return boardPanel.getDisplayFrame();
		
	}


	public void update() {
		
	}
	
	public BoardPanel getBoardPanel() {
		return boardPanel;
	}
	
	public Observer getObservable() {
		return boardPanel.getNoCar();
	}


}
