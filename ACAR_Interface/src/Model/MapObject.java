package Model;

import java.awt.image.BufferedImage;

public class MapObject {
	private int x;
	private int y;
	private BufferedImage sprite;
	
	public int getX() {
		return this.x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public BufferedImage getSprite() {
		return this.sprite;
	}
	
	public void setSprite(BufferedImage sprite) {
		this.sprite = sprite;
	}
}
