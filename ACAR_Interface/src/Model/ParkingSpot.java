package Model;

public class ParkingSpot extends MapObject{
	private float length;
	private Direction direction;

	public float getLength() {
		return length;
	}

	public void setLength(float length) {
		this.length = length;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}
}
