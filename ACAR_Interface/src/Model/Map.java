package Model;

public class Map implements IModel{
	private MapObject mapObject;

	public MapObject getMapObject() {
		return this.mapObject;
	}

	public void setMapObject(MapObject mapObject) {
		this.mapObject = mapObject;
	}
}
