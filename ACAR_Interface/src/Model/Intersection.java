package Model;

public class Intersection extends MapObject{
	private int id;
	private Intersection[] neighbors;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Intersection[] getNeighbors() {
		return neighbors;
	}
	
	public void setNeighbors(Intersection[] neighbors) {
		this.neighbors = neighbors;
	}
}
