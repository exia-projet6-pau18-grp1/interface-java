package Model;

public interface IModel {
	public MapObject getMapObject();
	public void setMapObject(MapObject mapObject);
}
